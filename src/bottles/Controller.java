package bottles;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

public class Controller {
    public Button insertBtn;
    public Button returnBtn;
    public TextArea statusArea;
    public ComboBox<Bottle> productSelectBox;
    public Label moneyLbl;
    public Button buyBtn;
    public Slider valueSlider;
    public Label valueLbl;
    public Button receiptBtn;

    private BottleDispenser bd = BottleDispenser.getInstance();
    private DecimalFormat df = new DecimalFormat("#0.00");
    private Bottle lastBought;

    public void initialize() {
        updateProductBox();
    }

    private void updateMoneyLbl() {
        moneyLbl.setText(df.format(bd.getMoney()) + " €");
    }

    public void updateValueLbl() {
        valueLbl.setText(df.format(valueSlider.getValue()) + " €");
    }

    private void updateProductBox() {
        productSelectBox.getItems().removeAll(productSelectBox.getItems());

        for (Bottle b : bd.getBottleArray()) {
            productSelectBox.getItems().add(b);
        }
    }

    private void printMessage(String msg) {
        statusArea.appendText(msg + System.lineSeparator());
    }

    public void insertMoney(ActionEvent actionEvent) {
        double value = valueSlider.getValue();

        valueSlider.setValue(0);
        updateValueLbl();

        printMessage(bd.addMoney(value));
        updateMoneyLbl();
    }

    public void returnChange(ActionEvent actionEvent) {
        printMessage(bd.returnMoney());
        updateMoneyLbl();
    }

    public void buyBottle(ActionEvent actionEvent) {
        Bottle temp = productSelectBox.getValue();
        if (bd.getMoney() >= temp.getPrice()) lastBought = temp;

        printMessage(bd.buyBottle(temp));
        updateMoneyLbl();
        updateProductBox();
    }

    public void printReceipt() throws IOException {
        if (lastBought != null) {
            BufferedWriter out = new BufferedWriter(new FileWriter("kuitti.txt"));
            out.write("Kuitti");
            out.newLine();
            out.write(lastBought.toString());
            out.newLine();
            out.close();
        }
    }
}
