package bottles;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by marti on 30.11.2016.
 */
public class BottleDispenser {
    private static BottleDispenser bd = null;

    private int bottles;
    private ArrayList<Bottle> bottle_array;
    private double money;

    private BottleDispenser() {
        bottles = 6;
        money = 0;

        // Initialize the array
        bottle_array = new ArrayList<>(bottles);
        // Add Bottle-objects to the array
        bottle_array.add(new Bottle("Pepsi Max", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
    }

    static BottleDispenser getInstance() {
        if (bd == null) bd = new BottleDispenser();
        return bd;
    }

    ArrayList<Bottle> getBottleArray() {
        return bottle_array;
    }

    double getMoney() {
        return money;
    }

    String addMoney(double amount) {
        money += amount;
        return "Klink! Lisää rahaa laitteeseen!";
    }

    String buyBottle(Bottle bottle) {
        String msg;
        if (money < bottle.getPrice()) {
            msg = "Syötä rahaa ensin!";
        } else {
            bottles -= 1;
            money -= bottle.getPrice();

            msg = "KACHUNK! " + bottle.getName() + " tipahti masiinasta!";
            deleteBottle(bottle);
        }

        return msg;
    }

    String returnMoney() {
        DecimalFormat df = new DecimalFormat("#0.00");
        double change = money;
        money = 0;
        return "Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df.format(change) + "€";

    }

    public void listBottles() {
        for (int i = 0; i < bottle_array.size(); i++) {
            Bottle bottle = bottle_array.get(i);
            System.out.println(i+1 + ". Nimi: " + bottle.getName());
            System.out.println("\tKoko: " + bottle.getSize() + "\tHinta: " + bottle.getPrice());
        }
    }

    private void deleteBottle(Bottle bottle) {
        bottle_array.remove(bottle);
    }
}