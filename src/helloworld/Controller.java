package helloworld;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    public Button button;
    public Label label;
    public TextField textfield;

    public void sayHello() {
        System.out.println("Hello World!");
        //label.setText("Hello World!");
        label.setText(textfield.getText());
    }

    public void updateText() {
        label.setText(textfield.getText());
    }
}
