package texteditor;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.*;

public class Controller {
    public Button saveBtn;
    public Button loadBtn;
    public TextArea textarea;
    public TextField filenameField;

    private String filename;

    public void save(ActionEvent actionEvent) throws IOException {
        filename = filenameField.getText();
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));

        out.write(textarea.getText());
        out.close();
    }

    public void load(ActionEvent actionEvent) throws IOException {
        filename = filenameField.getText();
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader in = new BufferedReader(new FileReader(filename));

        while ((line = in.readLine()) != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
        }

        String allText = sb.toString();
        textarea.setText(allText);
    }
}
